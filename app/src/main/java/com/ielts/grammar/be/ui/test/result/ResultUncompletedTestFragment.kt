package com.ielts.grammar.be.ui.test.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ielts.grammar.be.R
import com.ielts.grammar.be.data.ResultTest
import kotlinx.android.synthetic.main.fragment_test_list.*


class ResultUncompletedTestFragment : androidx.fragment.app.Fragment() {
    private lateinit var resultTestList: List<ResultTest>

    companion object {
        fun newInstance(testList: List<ResultTest>): ResultUncompletedTestFragment {
            val testListFragment = ResultUncompletedTestFragment()
            testListFragment.resultTestList = testList
            return testListFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.fragment_test_list, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val testListAdapter = ResultTestAdapter(activity!!, resultTestList, false)
        test_list_recycler_view.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        test_list_recycler_view.adapter = testListAdapter
    }

}

